(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("317754d03bb6d85b5a598480e1bbee211335bbf496d441af4992bbf1e777579e" "95e934b092694a2624adb653043d1dc016a6712fa27b788f9ff4dffb8ee08290" "6420b1fa49b2a96b00ca4c901b6093129ea35da475389e34399f192040eb9989" "a9eeab09d61fef94084a95f82557e147d9630fbbb82a837f971f83e66e21e5ad" "011f2e8d17b3e2d3a17b2da5188f358a79cedc184af29d3b1d31ce8c4ca1aa65" "eb89ed96edbf00d3cdf435e2c2dbbb1b9ad86c44004885875d791045ca80424d" "da5be529e618328342956804dff6db8d4ca3bcd0e4898668bb18d717c0834bce" "9024f09124de3263e892a65045dbd68ce02a5c5451ef6e8158cda163d4ab9f2e" "f7c2f2aec63e8e38f402cb2e5d5dd1b35dd17b946f0c41e905ca3c163f4c3cee" "b906c15b69a3d9d73ca4876baa260a0b51c32cfd865ce698259bf71a98e3f555" "2a309fcf0e41e45010501ecf1931cb47638123be5563e2df349b7e38a80696fd" "4853d3e18587e4ca9a83f19662a25a4fca369d36ffccbae4b4151873792a555b" "fdabe6ac6d4c01801368327c400161ed8e0bf4b65d5000c37b8aba543fac0cc1" "8d994715356726cf6ac9c7f0a1a51f12c08cadfc35caaf96e91e7b7f98d48d1a" "2756f31139b3aba216ecaa0c756c0ba3b04990c493d2a4d1b9ec5d89880bb772" "64d55805fe3fb0938f9f3a21358126d1ba60e162475b6c452e38d1e31d9f5962" "d29dcf2d206b2e4b01e8ca90732348ecf35bcf068c319fe4805168dfa21ed3e1" "e09bb5752ed7cdc19c72cdaaefce93d72b9aac4736c0a8ea58b47158c8d420f8" "6bdc4e5f585bb4a500ea38f563ecf126570b9ab3be0598bdf607034bb07a8875" "12809de646a84fa4c5345defaf26b200d67cb5ddd0a3454c78d14ebdeee38901" "9f297216c88ca3f47e5f10f8bd884ab24ac5bc9d884f0f23589b0a46a608fe14" "6e33d3dd48bc8ed38fd501e84067d3c74dfabbfc6d345a92e24f39473096da3f" "1f6f20bf65b5d653ba25e076815cc42450fd6b9daaa2bd50c2a2ce8f3b9a635e" "186f284f96a85c2ad00c6043fdba464e0b1c64568fdaa46d92abc25f22fd8043" "fb8a3c0c1c26cd6c20c543113f0cfd14caa7b4a40d38ddee8e42c70b0c78cdbd" default))
 '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(mode-line-format
   '("%e" mode-line-front-space
     (:propertize
      ("" mode-line-mule-info mode-line-client mode-line-modified mode-line-remote)
      display
      (min-width
       (5.0)))
     mode-line-frame-identification mode-line-buffer-identification "   " mode-line-position
     (vc-mode vc-mode)
     "  " mode-line-misc-info mode-line-end-spaces))
 '(package-selected-packages '(color-theme-sanityinc-tomorrow powerline)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(vterm-color-black ((t (:background "#808080" :foreground "#808080"))))
 '(vterm-color-bright-black ((t (:inherit vterm-color-black)))))
(put 'set-goal-column 'disabled nil)
