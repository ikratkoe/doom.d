(setq display-line-numbers-type 'relative)
(setq doom-font (font-spec :weight 'Regular :family "xos4Terminus" :size 10))
(setq org-directory "~/life")

(add-hook! '+doom-dashboard-mode-hook (hl-line-mode -1))
(setq-hook! '+doom-dashboard-mode-hook evil-normal-state-cursor (list nil))
(setq fancy-splash-image "~/.config/doom/emacs.svg")
(add-hook! '+doom-dashboard-functions (hide-mode-line-mode 1))
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-footer)
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)

(setq doom-theme 'doom-solarized-light)

(keymap-set global-map "C-x t" nil)
;;dj fake b2b dj set (mix 4 P.K.)

(map! "M-g c" #'goto-char)
(map! "M-g M-c" #'avy-goto-char)
(map! "M-g M-g" #'avy-goto-line)
(map! "M-g g" #'goto-line)

(map! "C-S-c C-S-c" #'mc/edit-lines)
(map! "C->" #'mc/mark-next-like-this)
(map! "C-<" #'mc/mark-previous-like-this)
(map! "C-c C-<" #'mc/mark-all-like-this)
(map! "C-\"" #'mc/skip-to-next-like-this)
(map! "C-:" #'mc/skip-to-previous-like-this)

(setq frame-resize-pixelwise t)

(after! gitlab-ci-mode-flycheck
  (gitlab-ci-mode-flycheck-enable))

(defun my/magit-display-buffer (buffer)
  (display-buffer
   buffer (if (derived-mode-p 'magit-mode)
              '(display-buffer-same-window)
            '(nil (inhibit-same-window . t)))))
(after! magit
  (setq magit-display-buffer-function #'my/magit-display-buffer))

(global-visual-line-mode -1)
(map! "M-s M-s" #'avy-goto-char)
(map! "M-s s" #'avy-goto-char)

(defun colorize-compilation-buffer ()
  (toggle-read-only)
  (ansi-color-apply-on-region compilation-filter-start (point))
  (toggle-read-only))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)
(setq company-format-margin-function nil)

(defun duplicate-line ()
  "Duplicate current line"
  (interactive)
  (let ((column (- (point) (point-at-bol)))
        (line (let ((s (thing-at-point 'line t)))
                (if s (string-remove-suffix "\n" s) ""))))
    (move-end-of-line 1)
    (newline)
    (insert line)
    (move-beginning-of-line 1)
    (forward-char column)))

(map! "C-," #'duplicate-line)
(setq +notmuch-sync-backend 'mbsync)
(setq +notmuch-home-function (lambda () (notmuch-search "tag:inbox")))
(setq vterm-disable-bold t)
(setq vterm-timer-delay nil)
